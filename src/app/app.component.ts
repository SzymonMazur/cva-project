import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Product } from './interfaces/product.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'my-app';
  form!: FormGroup;

  productsArray: Product[] = [
    { id: 1, name: 'Produkt 1', description: 'Our best product 1' },
    { id: 2, name: 'Produkt 2', description: 'Our best product 2' },
    { id: 3, name: 'Produkt 3', description: 'Our best product 3' },
    { id: 4, name: 'Produkt 4', description: 'Our best product 4' },
    { id: 5, name: 'Produkt 5', description: 'Our best product 5' },
    { id: 6, name: 'Produkt 6', description: 'Our best product 6' },
  ];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      product: '',
    });
  }
}
