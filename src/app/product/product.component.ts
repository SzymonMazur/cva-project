import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Product } from '../interfaces/product.interface';
import {
  faChevronUp,
  faChevronDown,
  faQuestionCircle,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProductComponent),
      multi: true,
    },
  ],
})
export class ProductComponent implements ControlValueAccessor {
  @Input() products: Product[] = [];

  isListVisible: boolean = false;
  isTooltipVisible: boolean[] = [];
  value!: Product;
  propagateChange: any = () => {};
  presentIcon = faChevronDown;
  tooltipIcon = faQuestionCircle;

  constructor() {}

  toggleList() {
    this.isListVisible = !this.isListVisible;
    this.presentIcon === faChevronDown
      ? (this.presentIcon = faChevronUp)
      : (this.presentIcon = faChevronDown);
  }

  showTooltip(idx: number) {
    this.isTooltipVisible[idx] = !this.isTooltipVisible[idx];
  }

  hideTooltip(idx: number) {
    this.isTooltipVisible[idx] = !this.isTooltipVisible[idx];
  }

  selectProduct(product: Product) {
    this.value = product;
    console.log(this.value);
    this.propagateChange(this.value);
  }

  getCssForSelectedProduct(product: Product) {
    return {
      selected: this.value && this.value.name === product.name,
    };
  }

  writeValue(value: any) {
    if (value) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    console.log(fn);
    this.propagateChange = fn;
  }

  registerOnTouched(): void {}
}
